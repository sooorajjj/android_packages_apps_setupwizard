package org.lineageos.setupwizard;

import android.os.Bundle;

import android.view.View;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ApplicationInfo;
// import android.content.pm.PackageInfo;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import org.lineageos.setupwizard.CustomAdapter;
import org.lineageos.setupwizard.AppInfo;
import android.graphics.drawable.Drawable;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class EAppsManagerActivity extends SubBaseActivity {

    public static final String TAG = EAppsManagerActivity.class.getSimpleName();


    @Override
    protected void onStartSubactivity() {
        setNextAllowed(true);
        listUninstallableApps();
        //launchAppsManager();
    }

    @Override
    protected int getTransition() {
        return TRANSITION_ID_SLIDE;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_eapps_manger;
    }

    @Override
    protected int getTitleResId() {
        return R.string.e_app_manager_setup_title;
    }

    @Override
    protected int getIconResId() {
        return R.drawable.ic_launcher;
    }

//    private void launchAccountManagerSetup() {
//        try {
//            accountManager = AccountManager.get(this);
//            accountManager.addAccount("e.foundation.webdav.eelo", null, null, null, this, new AccountManagerCallback<Bundle>() {
//                @Override
//                public void run(AccountManagerFuture<Bundle> future) {
//                    // An eelo account has been added, continue to the next screen
//                    onNavigateNext();
//                }
//            }, null);
//        }
//        catch (Exception e) {}
//        finally {}
//    }
    private void listUninstallableApps() {

        PackageManager pm = getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        List<String> appNames = new ArrayList<>();
        List<String> packageNames = new ArrayList<>();
        List<Integer> appIcons = new ArrayList<>();
        ApplicationInfo app;

        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.sourceDir.startsWith("/data/app/")) {
                AppInfo newApp = new AppInfo();
                Log.e(TAG, "Installed package :" + packageInfo.packageName);
                Log.e(TAG, "Source dir : " + packageInfo.sourceDir);
                Log.e(TAG, "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));
                try {
                    app = pm.getApplicationInfo(packageInfo.packageName, PackageManager.GET_META_DATA);
                } catch (final NameNotFoundException e) {
                    app = null;
                }
                String appName = (String) (app != null ? pm.getApplicationLabel(app) : "(unknown)");

                appNames.add(appName);
                packageNames.add(packageInfo.packageName);
                try {
                    Drawable icon = pm.getApplicationIcon(packageInfo.packageName);
                    newApp.setAppIcon(icon);
                } catch (final NameNotFoundException e) {
                    //icon = ContextCompat.getDrawable(mContext, R.drawable.ic_launcher_background);
                }
                //appIcons.add(icon);
                //imageView.setImageDrawable(icon);
            }
        }
        //uninstallApp("foundation.e.calendar");
        launchAppsManager(appNames,packageNames);
    }

    // private void uninstallApp(String packageName) {
    //     Log.e(TAG, "Uninstalling... " + packageName);
    //     Intent intent = new Intent(Intent.ACTION_DELETE);
    //     intent.setData(Uri.parse("package:"+packageName));
    //     startActivity(intent);
    // }


    private void launchAppsManager(List<String> appNames,List<String> packageNames) {
        try {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.app_list);
            // use this setting to
            // improve performance if you know that changes
            // in content do not change the layout size
            // of the RecyclerView
            recyclerView.setHasFixedSize(true);
            // use a linear layout manager
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            CustomAdapter mAdapter = new CustomAdapter(appNames,packageNames);
            recyclerView.setAdapter(mAdapter);

        }
        catch (Exception e) {}
        finally {}
    }

    @Override
    protected int getSubactivityNextTransition() {
        return TRANSITION_ID_SLIDE;
    }



}
