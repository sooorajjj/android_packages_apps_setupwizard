package org.lineageos.setupwizard;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageView;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import org.lineageos.setupwizard.AppInfo;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    private List<String> appNames;
    private List<String> packageNames;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imgAppIcon;
        public TextView txtAppName;
        public Button btnUnInstall;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            imgAppIcon = v.findViewById(R.id.icon);
            txtAppName = v.findViewById(R.id.app_name);
            btnUnInstall = v.findViewById(R.id.UnInstall);
        }
    }


    public void add(int position, String item) {
        appNames.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        appNames.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public  CustomAdapter(List<String> AppNames, List<String> PackageNames) {
        appNames = AppNames;
        packageNames = PackageNames;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v =
                inflater.inflate(R.layout.list_apps, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final String appName = appNames.get(position);
        final String packageName = packageNames.get(position);


        holder.txtAppName.setText(appName);

        holder.btnUnInstall.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
				Log.e("EAppsManagerActivity", "Uninstalling... :" + packageName);
		        Intent intent = new Intent(Intent.ACTION_DELETE);
		        intent.setData(Uri.parse("package:"+packageName));
		        v.getContext().startActivity(intent);

		        //TODO : fix the view after uninstall success
		        // remove(position); // Dont remove the item here
        	}
        });

        // holder.imgAppIcon.setImageDrawable(appIcon);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return appNames.size();
    }


}